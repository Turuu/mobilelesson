package com.example.altantur.flappyi;

import com.example.altantur.flappyi.model.Bird;
import com.example.altantur.flappyi.model.Obstacle;
import com.example.altantur.flappyi.model.Track;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;

import static android.content.Context.MODE_PRIVATE;

/**
 * @author impaler
 * This is the main surface that handles the ontouch events and draws
 * the image to the screen.
 */
public class MainGamePanel extends SurfaceView implements SurfaceHolder.Callback {

    private static final String TAG = MainGamePanel.class.getSimpleName();
    private MainThread thread;
    private Bird bird;
    private Track zam;
    private boolean isGameStarted;
    private Obstacle[] twin;
    private Bitmap background;
    private int obs_speed;
    private boolean turn;
    private int width;
    private int height;
    private boolean isGG;
    private  boolean whatet;
    private  Context context;
    int x;
    int y;
    public MainGamePanel(Context context) {
        super(context);
        // adding the callback (this) to the surface holder to intercept events
        getHolder().addCallback(this);
        this.context = context;

        DisplayMetrics displayMetrics = context.getResources().getDisplayMetrics();
        width = displayMetrics.widthPixels;
        height = displayMetrics.heightPixels;
        x = width / 5 * 2;
        y = height / 3;
        obs_speed = 9;
        //Getting background and formatting to any screen
        Bitmap bmp = BitmapFactory.decodeResource(getResources(), R.drawable.bg);
        background = Bitmap.createBitmap(getResizedBitmap(bmp, height, width));
        isGG = false;
        //Which obstacles could happen
        turn = false;
        whatet =false;

        // Get track on
        bmp = BitmapFactory.decodeResource(getResources(), R.drawable.zam);
        bmp = getResizedBitmap(bmp, height/7, bmp.getWidth());
        zam = new Track(bmp, 0, height-bmp.getHeight()/5, obs_speed);

        // Link obstacles
        Bitmap top = BitmapFactory.decodeResource(getResources(), R.drawable.u);
        Bitmap down = BitmapFactory.decodeResource(getResources(), R.drawable.d);
        twin = new Obstacle[2];
        twin[0]= new Obstacle(getResizedBitmap(top, width/5 * 6, width/5), getResizedBitmap(down, width/5 * 6, width/5), width, height, obs_speed);
        twin[1]= new Obstacle(getResizedBitmap(top, width/5 * 6, width/5), getResizedBitmap(down, width/5 * 6, width/5), width, height, obs_speed);

        // create bird and load bitmap
        Bitmap temp = BitmapFactory.decodeResource(getResources(), R.drawable.droid_1);
        bird = new Bird(getResizedBitmap(temp, width/6, width/6), x, y, height-bmp.getHeight()/5);

        // Set starting point
        isGameStarted = false;

        // create the game loop thread
        thread = new MainThread(getHolder(), this);

        // make the GamePanel focusable so it can handle events
        setFocusable(true);
    }

    @Override
    public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {
    }

    @Override
    public void surfaceCreated(SurfaceHolder holder) {
        // at this point the surface is created and
        // we can safely start the game loop
        thread.setRunning(true);
        thread.start();
    }

    @Override
    public void surfaceDestroyed(SurfaceHolder holder) {
        Log.d(TAG, "Surface is being destroyed");
        // tell the thread to shut down and wait for it to finish
        // this is a clean shutdown
        boolean retry = true;
        while (retry) {
            try {
                thread.join();
                retry = false;
            } catch (InterruptedException e) {
                // try again shutting down the thread
            }
        }
        Log.d(TAG, "Thread was shut down cleanly");
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        if (event.getAction() == MotionEvent.ACTION_DOWN) {
            if(isGameStarted)
                bird.handleActionDown();
            else isGameStarted = true;
            /*if(isGG){
                Obstacle.setPoint(0);
                twin[0].calcPos();
                twin[1].calcPos();
                isGG = false;
                //Which obstacles could happen
                turn = false;
                whatet =false;
                bird.setX(x);
                bird.setY(y);
            }*/

        }
        if (event.getAction() == MotionEvent.ACTION_MOVE) {
        }
        if (event.getAction() == MotionEvent.ACTION_UP) {
        }
        return true;
    }

    public void render(Canvas canvas) {
        canvas.drawBitmap(background, 0, 0, null);
        Paint fillPaint = new Paint();
        fillPaint.setColor(Color.BLACK);
        fillPaint.setTextSize(width/8);

        if(!isGG)
        if(!whatet)
            canvas.drawText("0", width/2 - width/16, height/4, fillPaint);
        else {
            canvas.drawText(Obstacle.getPoint() + "", width/2 - width/16, height/4, fillPaint);
        }
        if(twin[0].isHalf()) whatet = true;

        bird.draw(canvas);
        twin[0].draw(canvas);
        if(turn) twin[1].draw(canvas);
        zam.draw(canvas);

        if(isGG){
            fillPaint.setTextSize(width/16);
            canvas.drawText("Best score : " + Obstacle.getBest(), width/3 - width/16, height/4, fillPaint);
            canvas.drawText("Your score : " + Obstacle.getPoint(), width/3 - width/16, height/3, fillPaint);
            canvas.drawText("Touch to restart" , width/3 - width/16, height/2, fillPaint);
        }
    }


    public void update() {

        if(twin[0].isCollision(bird) || twin[1].isCollision(bird)){
            isGameStarted = false;
            isGG = true;
            String best = readFromFile();
            if(best == null) Obstacle.setBest(Obstacle.getPoint());
            else {
                int temp = Integer.parseInt(best);
                if(Obstacle.getPoint() > temp)
                    Obstacle.setBest(Obstacle.getPoint());
                else Obstacle.setBest(temp);
            }
            writeToFile(Obstacle.getBest()+"");

        }

        if(twin[0].getX() < width/2)
            turn = true;
        // Update the lone bird along obstacles
        if (isGameStarted) {
            bird.update();
            twin[0].update();
            if(turn) twin[1].update();
        }
        zam.update();
    }

    public Bitmap getResizedBitmap(Bitmap bm, int newHeight, int newWidth) {
        int width = bm.getWidth();
        int height = bm.getHeight();
        float scaleWidth = ((float) newWidth) / width;
        float scaleHeight = ((float) newHeight) / height;
        Matrix matrix = new Matrix();
        matrix.postScale(scaleWidth, scaleHeight);
        Bitmap resizedBitmap = Bitmap.createBitmap(bm, 0, 0, width, height, matrix, false);
        return resizedBitmap;
    }

    private void writeToFile(String data) {
        try {
            OutputStreamWriter outputStreamWriter = new OutputStreamWriter(context.openFileOutput("config.txt", Context.MODE_PRIVATE));
            outputStreamWriter.write(data);
            outputStreamWriter.close();
        }
        catch (IOException e) {
            Log.e("Exception", "File write failed: " + e.toString());
        }
    }

    private String readFromFile() {

        String ret = null;

        try {
            InputStream inputStream = context.openFileInput("config.txt");

            if ( inputStream != null ) {
                InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
                BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
                String receiveString = "";
                StringBuilder stringBuilder = new StringBuilder();

                while ( (receiveString = bufferedReader.readLine()) != null ) {
                    stringBuilder.append(receiveString);
                }

                inputStream.close();
                ret = stringBuilder.toString();
            }
        }
        catch (FileNotFoundException e) {
            Log.e("login activity", "File not found: " + e.toString());
        } catch (IOException e) {
            Log.e("login activity", "Can not read file: " + e.toString());
        }
        return ret;
    }

}

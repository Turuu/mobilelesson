package com.example.altantur.flappyi.model.component;

public class Speed {

    private float yVelocity = 0;	// velocity value on the Y axis
    private float acceleration = 2;	// acceleration value on the Y axis

    public Speed() {
    }

    public Speed(float yv) {
        this.yVelocity = yv;
    }

    public void setyVelocity(float velocity){
        this.yVelocity = velocity;
    }

    public float getYVelocity() {
        return yVelocity;
    }

    public void accel(){
        yVelocity += acceleration;
    }
}

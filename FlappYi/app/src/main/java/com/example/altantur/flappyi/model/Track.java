package com.example.altantur.flappyi.model;

import android.graphics.Bitmap;
import android.graphics.Canvas;

public class Track {

    private Bitmap zam;
    private int x;
    private int y;
    private int velo;

    public Track(Bitmap zam, int x, int y, int velo) {
        this.zam = zam;
        this.velo = velo;
        this.x = x;
        this.y = y;
    }

    public void draw(Canvas canvas) {
        canvas.drawBitmap(zam, x - (zam.getWidth() / 2), y - (zam.getHeight() / 2), null);
    }

    public void update() {
        x -= velo;
        if(x < -900) x = 0;
    }

}

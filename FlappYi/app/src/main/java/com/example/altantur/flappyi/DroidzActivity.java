package com.example.altantur.flappyi;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.Window;
import android.view.WindowManager;

import java.io.IOException;
import java.io.OutputStreamWriter;

public class DroidzActivity extends Activity {
    /** Called when the activity is first created. */

    private static final String TAG = DroidzActivity.class.getSimpleName();
    private Context context;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // requesting to turn the title OFF
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        // making it full screen
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        // set our MainGamePanel as the View
        setContentView(new MainGamePanel(this));
        Log.d(TAG, "View added");
        context = getApplicationContext();
    }

    @Override
    protected void onDestroy() {
        Log.d(TAG, "Destroying...");
        
        super.onDestroy();
        finish();
    }

    @Override
    protected void onStop() {
        Log.d(TAG, "Stopping...");

        super.onStop();
        finish();
    }


    private void writeToFile(String data) {
        try {
            OutputStreamWriter outputStreamWriter = new OutputStreamWriter(openFileOutput("scores.txt", Context.MODE_PRIVATE));
            outputStreamWriter.write(data);
            outputStreamWriter.close();
        }
        catch (IOException e) {
            Log.e("Exception", "File write failed: " + e.toString());
        }
    }

}
package com.example.altantur.flappyi.model;

import com.example.altantur.flappyi.model.component.Speed;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.util.Log;
import android.view.MotionEvent;

/**
 * This is a test droid that is dragged, dropped, moved, smashed against
 * the wall and done other terrible things with.
 * Wait till it gets a weapon!
 * @author impaler
 */

public class Bird {

    private Bitmap bitmap;	// the actual bitmap
    private int x;			// the X coordinate
    private int y;			// the Y coordinate
    private Speed speed;	// the speed with its vertically
    private int max_y;     // set bottom limit
    private boolean isDead;

    public Bird(Bitmap bitmap, int x, int y, int height) {
        this.bitmap = bitmap;
        this.x = x;
        this.y = y;
        this.speed = new Speed();
        this.max_y = height - bitmap.getHeight()- bitmap.getHeight()/4;
        this.isDead = false;
    }

    public Bitmap getBitmap() {
        return bitmap;
    }
    public void setBitmap(Bitmap bitmap) {
        this.bitmap = bitmap;
    }
    public int getX() {
        return x;
    }
    public void setX(int x) {
        this.x = x;
    }
    public int getY() {
        return y;
    }
    public void setY(int y) {
        this.y = y;
    }


    public Speed getSpeed() {
        return speed;
    }

    public void setSpeed(Speed speed) {
        this.speed = speed;
    }

    public void draw(Canvas canvas) {
        canvas.drawBitmap(bitmap, x - (bitmap.getWidth() / 2), y - (bitmap.getHeight() / 2), null);
    }

    public void update() {
            if(y < max_y){
                y += speed.getYVelocity();
                speed.accel();
                isDead = true;
            }
    }

    public void handleActionDown() {
        speed.setyVelocity(-35);
    }
}

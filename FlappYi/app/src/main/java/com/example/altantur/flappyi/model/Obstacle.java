package com.example.altantur.flappyi.model;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.util.Log;

import java.util.Random;

public class Obstacle {
    private Bitmap obTop;
    private Bitmap obDown;
    private int mapWidth;
    private int mapHeight;
    private int width;
    private int height;
    private int x;
    private int yT;
    private int yD;
    private int velo;
    private static int point = 1;



    private static int best = 0;
    private boolean isStarted;

    public Obstacle(Bitmap obTop, Bitmap obDown, int width, int height, int velo) {
        this.obTop = obTop;
        this.obDown = obDown;
        this.velo = velo;
        this.mapWidth = width;
        this.mapHeight = height;
        this.width = obTop.getWidth();
        this.height = obTop.getHeight();
        this.isStarted = false;
        this.calcPos();
    }

    public static int getBest() {
        return best;
    }

    public static void setBest(int best) {
        Obstacle.best = best;
    }

    public void calcPos(){
        Random rand = new Random();
        int positioner = rand.nextInt(height / 3 * 2);
        this.x = mapWidth + width/2;
        positioner = mapHeight / 15 + positioner;
        yT = positioner - height + (height / 2);
    }



    public int getX() {
        return x;
    }
    public void setX(int x) {
        this.x = x;
    }
    public int getyT() {
        return yT;
    }
    public void setyT(int y) {
        this.yT = y;
    }
    public int getyD() {
        return yD;
    }
    public void setyD(int y) {
        this.yD = y;
    }

    public boolean isHalf(){
        if (x - (width / 2) < mapWidth/2) return true;
        return false;
    }

    public void draw(Canvas canvas) {
        canvas.drawBitmap(obTop, x - (width / 2), yT - (height / 2), null);
        canvas.drawBitmap(obDown, x - (width / 2), yD + yT + height, null);
    }

    public static int getPoint(){
        return point;
    }

    public static void setPoint(int point){
        Obstacle.point = point;
    }

    public void update() {
        x -= velo;
        if (x < -1 * width) {
            this.calcPos();
            point ++;
            Log.i("This is point:", point+"");
        }
    }

    public boolean isCollision(Bird bird){
        int bx = bird.getX(), by = bird.getY(), bWidth = bird.getBitmap().getWidth();
        // Collision detection
        if (by-bWidth/2 < yT + height/2 || by+bWidth/2 > yD + yT + height){
            if(bx + bWidth / 2 > x - (width / 2) && bx - (bWidth / 2) < x + (width / 2))
                return true;
        }
            return  false;
    }

    public static boolean intervallContains(int low, int high, int n) {
        return n >= low && n <= high;
    }

}

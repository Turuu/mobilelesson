package com.example.altantur.daalgawar3;

import android.content.Intent;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;


public class thirdActivity extends ActionBarActivity {
    Button butsah;
    Button garah;
    Intent buts;
    TextView from;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_third);
        butsah = (Button)findViewById(R.id.button2);
        garah = (Button)findViewById(R.id.button3);
        Bundle personData = getIntent().getExtras();
        String prevActiv = personData.getString("activity");
        Toast.makeText(getApplicationContext(), prevActiv, Toast.LENGTH_LONG);
        from = (TextView) findViewById(R.id.from);
        from.setText(prevActiv);
        buts = getIntent();
        butsah.setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                buts.putExtra("activity", "thirdActivity");
                setResult(RESULT_OK, buts);
                finish();
            }
        });
        garah.setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                setResult(RESULT_CANCELED, buts);
                finish();
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_third, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}

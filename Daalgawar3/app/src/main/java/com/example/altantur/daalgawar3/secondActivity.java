package com.example.altantur.daalgawar3;

import android.content.Intent;
import android.media.ExifInterface;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;


public class secondActivity extends ActionBarActivity {
    Button butsah;
    Button garah;
    Button guraw;
    Intent buts;
    int A1 = 123;
    TextView from;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_second);
        butsah = (Button)findViewById(R.id.button2);
        garah = (Button)findViewById(R.id.button3);
        guraw = (Button)findViewById(R.id.button);
        Bundle personData = getIntent().getExtras();
        String prevActiv = personData.getString("activity");
        Toast.makeText(getApplicationContext(), prevActiv, Toast.LENGTH_LONG);
        from = (TextView) findViewById(R.id.from);
        from.setText(prevActiv);
        buts = getIntent();
        butsah.setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                buts.putExtra("activity", "secondActivity");
                setResult(RESULT_OK, buts);
                finish();
            }
        });
        garah.setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                setResult(RESULT_CANCELED, buts);
                finish();
            }
        });
        guraw.setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {

                Intent go = new Intent();
                go.putExtra("activity", "Second Activity");
                go.setClassName("com.example.altantur.daalgawar3",
                        "com.example.altantur.daalgawar3.thirdActivity");
                startActivityForResult(go, A1);
            }
        });
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_second, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == A1) {
            switch (resultCode) {
                case RESULT_OK:
                    String prev = data.getStringExtra("activity");
                    from.setText(prev);
                    break;
                case RESULT_CANCELED:
                    break;
            }
        }
    }

}

package com.example.altantur.daalgawar3;

import android.content.Intent;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;


public class firstActivity extends ActionBarActivity {
    private static final int A1 = 123, A2 = 124, A3 = 125, A4 = 126, A5 = 127;
    private static final int MACT1 = Menu.FIRST;
    private static final int MACT2 = Menu.FIRST + 1;
    private static final int MACT3 = MACT2 + 1;
    TextView from;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_first);
        from = (TextView)findViewById(R.id.from);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_first, menu);
        menu.add(0, MACT2, 0, R.string.act2);
        menu.add(0, MACT3, 0, R.string.act3);
        return true;

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }
        Intent go = new Intent();
        switch (id) {
            case MACT1:
                break;
            case MACT2:
                go.putExtra("activity", "firstActivity");
                go.setClassName("com.example.altantur.daalgawar3",
                        "com.example.altantur.daalgawar3.secondActivity");
                startActivityForResult(go, A1);
                break;
            case MACT3:
                go.putExtra("activity", "firstActivity");
                go.setClassName("com.example.altantur.daalgawar3",
                        "com.example.altantur.daalgawar3.thirdActivity");
                startActivityForResult(go, A2);
                break;
            default:
                break;
        }

        return super.onOptionsItemSelected(item);
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == A1) {
            switch (resultCode) {
                case RESULT_OK:
                    String prev = data.getStringExtra("activity");
                    from.setText(prev);
                    break;
                case RESULT_CANCELED:
                    finish();
                    break;
            }
        }
    }
}

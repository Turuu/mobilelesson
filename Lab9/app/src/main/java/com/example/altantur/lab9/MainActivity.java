package com.example.altantur.lab9;

import android.app.Activity;
import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;


public class MainActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        //View shapes = new SimpleDrawing(this);
        //setContentView(shapes);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public class SimpleDrawing extends View {
        int starSize = 42;
        Bitmap star = Bitmap.createBitmap(this.starSize, this.starSize,
                Bitmap.Config.ARGB_8888);
        private final Paint basicPaint = new Paint();
        public SimpleDrawing(Context context) {
            super(context);
            Resources resource = this.getContext().getResources();
            Drawable image = resource.getDrawable(R.drawable.ic_launcher);
            Canvas canvas = new Canvas(this.star);
            image.setBounds(0, 0, this.starSize, this.starSize);
            image.draw(canvas);
        }
        @Override
        protected void onDraw(Canvas canvas)
        { canvas.drawColor(Color.BLACK);
            for (int topRight = 0; topRight < 3000; topRight += this.starSize)
                canvas.drawBitmap(this.star, topRight, topRight, this.basicPaint);
        }
    }
}

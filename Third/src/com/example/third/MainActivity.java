package com.example.third;

import java.util.Calendar;

import android.support.v7.app.ActionBarActivity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

public class MainActivity extends ActionBarActivity {
	/*
	 * Primary variable for common places
	 */
	private static final int A1 = 123, A2 = 124, A3 = 125, A4 = 126, A5 = 127;
	Button act1;
	Button act2;
	Button act3;
	Button act4;
	Button act5;
	private static final int MACT1 = Menu.FIRST;
	private static final int MACT2 = Menu.FIRST + 1;
	private static final int MACT3 = MACT2 + 1;
	private static final int MACT4 = MACT3 + 1;
	private static final int MACT5 = MACT4 + 1;

	/*
	 * Variables for data saving only in RAM
	 */
	String age = "";
	String name = "";
	String rating = "3.0";
	String rating1 = "3.0";
	int year, month, day, sex = 0, hour, minute, hour1, minute1, hour2,
			minute2;
	final Calendar c = Calendar.getInstance();

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.lab3);
		
		act1 = (Button) findViewById(R.id.btAct1);
		act2 = (Button) findViewById(R.id.btAct2);
		act3 = (Button) findViewById(R.id.btAct3);
		act4 = (Button) findViewById(R.id.btAct4);
		act5 = (Button) findViewById(R.id.btAct5);
		
		act1.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				Intent go = new Intent();
				go.putExtra("age", age);
				go.putExtra("name", name);
				go.setClassName("com.example.third",
						"com.example.third.Activity1");
				startActivityForResult(go, A1);
			}
		});
		/*
		 * Action for Activity 2
		 */
		
		act2.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				Intent go = new Intent();
				go.putExtra("year", year);
				go.putExtra("month", month);
				go.putExtra("day", day);

				go.setClassName("com.example.third",
						"com.example.third.Activity2");
				startActivityForResult(go, A2);
			}
		});

		act3.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				Intent go = new Intent();
				go.putExtra("sex", sex);
				go.putExtra("hour", hour);
				go.putExtra("minute", minute);
				go.setClassName("com.example.third",
						"com.example.third.Activity3");
				startActivityForResult(go, A3);
			}
		});
		
		act4.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				Intent go = new Intent();
				go.putExtra("rating", rating);
				go.putExtra("hour1", hour1);
				go.putExtra("minute1", minute1);
				go.setClassName("com.example.third",
						"com.example.third.Activity4");
				startActivityForResult(go, A4);
			}
		});
		
		act5.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				Intent go = new Intent();
				go.putExtra("rating1", rating1);
				go.putExtra("hour2", hour2);
				go.putExtra("minute2", minute2);
				go.setClassName("com.example.third",
						"com.example.third.Activity5");
				startActivityForResult(go, A5);
			}
		});
	}

	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (requestCode == A1) {
			switch (resultCode) {
			case RESULT_OK:
				age = data.getStringExtra("age");
				name = data.getStringExtra("name");
				Toast.makeText(getApplicationContext(),
						"Name: " + name + "  Age : " + age, Toast.LENGTH_LONG)
						.show();
				break;
			case RESULT_CANCELED:
				Toast.makeText(getApplicationContext(),
						"Kein Data zurückgegeben", Toast.LENGTH_LONG).show();
				break;
			}
		} else if (requestCode == A2) {
			switch (resultCode) {
			case RESULT_OK:
				year = data.getIntExtra("year", c.get(Calendar.YEAR));
				month = data.getIntExtra("month", c.get(Calendar.MONTH));
				day = data.getIntExtra("day", c.get(Calendar.DAY_OF_MONTH));
				int tempm = month + 1;
				Toast.makeText(
						getApplicationContext(),
						"Year:" + year + "  Month: " + tempm + "  Day:  " + day,
						Toast.LENGTH_LONG).show();
				break;
			case RESULT_CANCELED:
				Toast.makeText(getApplicationContext(),
						"Kein Data zurückgegeben", Toast.LENGTH_LONG).show();
				break;
			}
		} else if (requestCode == A3) {
			switch (resultCode) {
			case RESULT_OK:
				sex = data.getIntExtra("sex", 0);
				hour = data.getIntExtra("hour", c.get(Calendar.HOUR));
				minute = data.getIntExtra("minute", c.get(Calendar.MINUTE));
				String gend = (sex == 1) ? "Female" : "Male";
				Toast.makeText(
						getApplicationContext(),
						"Name: " + gend + "  Hour:  " + hour + "  Min: "
								+ minute, Toast.LENGTH_LONG).show();
				break;
			case RESULT_CANCELED:
				Toast.makeText(getApplicationContext(),
						"Kein Data zurückgegeben", Toast.LENGTH_LONG).show();
				break;
			}
		} else if (requestCode == A4) {
			switch (resultCode) {
			case RESULT_OK:
				hour1 = data.getIntExtra("hour1", c.get(Calendar.HOUR));
				minute1 = data.getIntExtra("minute1", c.get(Calendar.MINUTE));
				rating = data.getStringExtra("rating");

				Toast.makeText(
						getApplicationContext(),
						"H: " + hour1 + "  M: " + minute1 + "  Rating: "
								+ rating, Toast.LENGTH_LONG).show();
				break;
			case RESULT_CANCELED:
				Toast.makeText(getApplicationContext(),
						"Kein Data zurückgegeben", Toast.LENGTH_LONG).show();
				break;
			}
		} else if (requestCode == A5) {
			switch (resultCode) {
			case RESULT_OK:
				hour2 = data.getIntExtra("hour2", c.get(Calendar.HOUR));
				minute2 = data.getIntExtra("minute2", c.get(Calendar.MINUTE));
				rating1 = data.getStringExtra("rating1");

				Toast.makeText(
						getApplicationContext(),
						"H: " + hour2 + "  M: " + minute2 + "  Rating: "
								+ rating1, Toast.LENGTH_LONG).show();
				break;
			case RESULT_CANCELED:
				Toast.makeText(getApplicationContext(),
						"Kein Data zurückgegeben", Toast.LENGTH_LONG).show();
				break;
			}
		} else
			return;

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		menu.add(0, MACT1, 0, R.string.act1);
		menu.add(0, MACT2, 0, R.string.act2);
		menu.add(0, MACT3, 0, R.string.act3);
		menu.add(0, MACT4, 0, R.string.act4);
		menu.add(0, MACT5, 0, R.string.act5);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		Intent go = new Intent();
		switch (id) {
		case MACT1:
			
			go.putExtra("age", age);
			go.putExtra("name", name);
			go.setAction("cs696.sender.add1");
			startActivityForResult(go, A1);
			break;
		case MACT2:
			go.putExtra("year", year);
			go.putExtra("month", month);
			go.putExtra("day", day);
			go.setAction("cs696.sender.add2");
			startActivityForResult(go, A2);
			break;
		case MACT3:
			go.putExtra("sex", sex);
			go.putExtra("hour", hour);
			go.putExtra("minute", minute);
			go.setAction("cs696.sender.add3");
			startActivityForResult(go, A3);
			break;
		case MACT4:
			go.putExtra("rating", rating);
			go.putExtra("hour1", hour1);
			go.putExtra("minute1", minute1);
			go.setAction("cs696.sender.add4");
			startActivityForResult(go, A4);
			break;
		case MACT5:
			go.putExtra("rating1", rating1);
			go.putExtra("hour2", hour2);
			go.putExtra("minute2", minute2);
			go.setAction("cs696.sender.add5");
			startActivityForResult(go, A5);
			break;
		default:
			break;
		}
		return super.onOptionsItemSelected(item);
	}
}

package com.example.third;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class Activity1 extends Activity {
	EditText name;
	EditText age;
	Button ok;
	Button can; 
	Intent butsah;
	String ner = "";
	String nas = "";
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.act1);
		name = (EditText)findViewById(R.id.name);
		age = (EditText)findViewById(R.id.age);
		ok = (Button)findViewById(R.id.ok);
		can = (Button)findViewById(R.id.cancel);
		butsah = getIntent();
		Bundle personData = getIntent().getExtras();
		ner = personData.getString("name");
		nas = personData.getString("age");
		if ((age != null) && (name != null)) {
			name.setText(ner);
			age.setText(nas);
		}
		
		ok.setOnClickListener(new View.OnClickListener() {
			public void onClick(View arg0) {
				if ((age != null) && (name != null)) {
				butsah.putExtra("name", name.getText().toString());
				butsah.putExtra("age", age.getText().toString());
				}else {
					butsah.putExtra("name", "");
					butsah.putExtra("age", "");
				}
				setResult(RESULT_OK, butsah);
				finish();
			}
		});
		can.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				setResult(RESULT_CANCELED, butsah);
				finish();
			}
		});
		
		}
}




package com.example.third;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RatingBar;
import android.widget.TimePicker;
import android.widget.Toast;

public class Activity5 extends Activity {
	Button ok;
	Button can; 
	Intent butsah;
	RatingBar rt;
	TimePicker timePicker1;
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.act5);
		ok = (Button)findViewById(R.id.ok);
		can = (Button)findViewById(R.id.cancel);
		timePicker1 = (TimePicker) findViewById(R.id.timePicker1);
		butsah = getIntent();
		Bundle personData = getIntent().getExtras();
		rt = (RatingBar) findViewById(R.id.ratingBar);
		rt.setRating((float) Double.parseDouble(personData.getString("rating1")));
		timePicker1.setCurrentHour(personData.getInt("hour2"));
		timePicker1.setCurrentMinute(personData.getInt("minute2"));
		
		ok.setOnClickListener(new View.OnClickListener() {
			public void onClick(View arg0) {
				butsah.putExtra("hour2", timePicker1.getCurrentHour());
				butsah.putExtra("minute2", timePicker1.getCurrentMinute());
				butsah.putExtra("rating1", String.valueOf(rt.getRating()));
				setResult(RESULT_OK, butsah);
				finish();
			}
		});
		can.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				setResult(RESULT_CANCELED, butsah);
				finish();
			}
		});
		
		}
}




package com.example.third;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TimePicker;
import android.widget.Toast;

public class Activity3 extends Activity {
	Button ok;
	Button can;
	Intent butsah;
	RadioGroup radioSexGroup;
	RadioButton radioSexButton;
	TimePicker timePicker1;
	int hour, minute;

	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.act3);
		ok = (Button) findViewById(R.id.ok);
		can = (Button) findViewById(R.id.cancel);
		butsah = getIntent();
		Bundle personData = getIntent().getExtras();
		timePicker1 = (TimePicker) findViewById(R.id.timePicker1);
		radioSexGroup = (RadioGroup) findViewById(R.id.radioGender);
		hour = personData.getInt("hour");
		minute = personData.getInt("minute");
		timePicker1.setCurrentHour(hour);
		timePicker1.setCurrentMinute(minute);

		if (personData.getInt("sex") == 0)
			radioSexButton = (RadioButton) findViewById(R.id.genderMale);
		else
			radioSexButton = (RadioButton) findViewById(R.id.genderFemale);

		radioSexButton.setChecked(true);
		radioSexButton.setSelected(true);
		ok.setOnClickListener(new View.OnClickListener() {
			public void onClick(View arg0) {
				int selectedId = radioSexGroup.getCheckedRadioButtonId();
				radioSexButton = (RadioButton) findViewById(selectedId);
				int huis = (radioSexButton.getText().toString().trim()
						.equals("Male")) ? 0 : 1;
				butsah.putExtra("sex", huis);
				butsah.putExtra("hour", timePicker1.getCurrentHour());
				butsah.putExtra("minute", timePicker1.getCurrentMinute());
				setResult(RESULT_OK, butsah);
				finish();
			}
		});

		can.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				setResult(RESULT_CANCELED, butsah);
				finish();
			}
		});

	}
}

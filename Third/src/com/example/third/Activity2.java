package com.example.third;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;

public class Activity2 extends Activity {
	Button ok;
	Button can; 
	Intent butsah;
	DatePicker onSar;
	int year, month, day;
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.act2);
		ok = (Button)findViewById(R.id.ok);
		can = (Button)findViewById(R.id.cancel);
		butsah = getIntent();
		Bundle personData = getIntent().getExtras();
		year = personData.getInt("year");
		month = personData.getInt("month");
		day = personData.getInt("day");
		onSar = (DatePicker) findViewById(R.id.onSar);
		onSar.init(year, month, day, null);
		
		ok.setOnClickListener(new View.OnClickListener() {
			public void onClick(View arg0) {
				year = onSar.getYear();
				month = onSar.getMonth();
				day = onSar.getDayOfMonth();
				butsah.putExtra("year", year);
				butsah.putExtra("month", month);
				butsah.putExtra("day", day);
				setResult(RESULT_OK, butsah);
				finish();
			}
		});
		can.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				setResult(RESULT_CANCELED, butsah);
				finish();
			}
		});
		
		}
}




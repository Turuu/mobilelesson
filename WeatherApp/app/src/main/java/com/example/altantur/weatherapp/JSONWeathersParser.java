package com.example.altantur.weatherapp;

/**
 * Created by altantur on 12/17/14.
 */
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class JSONWeathersParser {

    public static Weather[] getWeather(String data) throws JSONException  {
        Weather[] weather = new Weather[8];
        Log.i("Data :", data);
        // We create out JSONObject from the data
        JSONObject jObj = new JSONObject(data);



        for (int i = 0; i < 8; i++){
            weather[i] = new Weather();

            // We get weather info (This is an array)
            JSONArray jArr = jObj.getJSONArray("list");
            Log.i("Fetched data", "data");

            // Weather overview
            JSONObject JSONWeather = jArr.getJSONObject(i);
            JSONArray wArr = JSONWeather.getJSONArray("weather");
            JSONObject weatherObj = wArr.getJSONObject(0);

            weather[i].currentCondition.setWeatherId(getInt("id", weatherObj));
            weather[i].currentCondition.setDescr(getString("description", weatherObj));
            weather[i].currentCondition.setCondition(getString("main", weatherObj));
            weather[i].currentCondition.setIcon(getString("icon", weatherObj));


            JSONObject mainObj = getObject("main", JSONWeather);
            weather[i].currentCondition.setHumidity(getInt("humidity", mainObj));
            weather[i].currentCondition.setPressure(getInt("pressure", mainObj));
            weather[i].temperature.setMaxTemp(getFloat("temp_max", mainObj));
            weather[i].temperature.setMinTemp(getFloat("temp_min", mainObj));
            weather[i].temperature.setTemp(getFloat("temp", mainObj));

            // Wind
            JSONObject wObj = getObject("wind", JSONWeather);
            weather[i].wind.setSpeed(getFloat("speed", wObj));
            weather[i].wind.setDeg(getFloat("deg", wObj));

            // Clouds
            JSONObject cObj = getObject("clouds", JSONWeather);
            weather[i].clouds.setPerc(getInt("all", cObj));

        }

        // We download the icon to show
        return weather;
    }


    private static JSONObject getObject(String tagName, JSONObject jObj)  throws JSONException {
        JSONObject subObj = jObj.getJSONObject(tagName);
        return subObj;
    }

    private static String getString(String tagName, JSONObject jObj) throws JSONException {
        return jObj.getString(tagName);
    }

    private static float  getFloat(String tagName, JSONObject jObj) throws JSONException {
        return (float) jObj.getDouble(tagName);
    }

    private static int  getInt(String tagName, JSONObject jObj) throws JSONException {
        return jObj.getInt(tagName);
    }

}
package com.example.altantur.weatherapp;

import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONException;
import org.xml.sax.InputSource;
import org.xml.sax.XMLReader;

import java.net.URI;
import java.net.URL;

import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;


public class MainActivity extends ActionBarActivity{
    static final String baseURL =
            "http://api.openweathermap.org/data/2.5/weather?q=";
    TextView cityText, tsagagaar;
    ImageView imgView;
    EditText city;
    Button weatherIt;
    int typer = 1;
    private static final int MACT1 = Menu.FIRST;
    private static final int MACT2 = Menu.FIRST + 1;
    private static final int MACT3 = MACT2 + 1;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        cityText = (TextView) findViewById(R.id.cityText);
        tsagagaar = (TextView) findViewById(R.id.weather);
        city = (EditText)findViewById(R.id.editText);
        weatherIt = (Button)findViewById(R.id.button);

        weatherIt.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
            }
        });
        weatherIt.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                String cityName = "London";
                if (city.getText() != null) {
                    cityName = city.getText().toString();
                }
                JSONWeatherTask task = new JSONWeatherTask();
                task.execute(new String[]{cityName});
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        menu.add(0, MACT1, 0, R.string.act1);
        menu.add(0, MACT2, 0, R.string.act2);
        menu.add(0, MACT3, 0, R.string.act3);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }
        switch (id) {
            case MACT1:
                if(isCityGiven())
                typer = 0;
                break;
            case MACT2:
                if(isCityGiven())
                typer = 1;
                break;
            case MACT3:
                if(isCityGiven())
                typer = 2;
                break;
            default:
                break;
        }

        return super.onOptionsItemSelected(item);
    }

    boolean isCityGiven(){
        if(city.getText().toString() != null)
        return true;
        return false;
    }

    class JSONWeatherTask extends AsyncTask<String, Void, Weather[]> {
        Weather[] temp = new Weather[8];
        @Override
        protected Weather[] doInBackground(String... params) {
            Weather weather = new Weather();
            String data = ( (new WeatherHttpClient()).getWeatherData(params[0], typer));
                try {
                    if(typer == 1){
                        weather = JSONWeatherParser.getWeather(data);
                        temp[0] = weather;
                    }
                    else if(typer == 2){
                        temp = JSONWeathersParser.getWeather(data);
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }

            return temp;

        }
        @Override
        protected void onPostExecute(Weather[] weather) {
            super.onPostExecute(weather);
            /*
            if (weather.iconData != null && weather.iconData.length > 0) {
                Bitmap img = BitmapFactory.decodeByteArray(weather.iconData, 0, weather.iconData.length);
                imgView.setImageBitmap(img);
            }*/
            if(typer == 1) {
                StringBuffer weatherText = new StringBuffer();
                cityText.setText(weather[0].location.getCity() + "," + weather[0].location.getCountry());
                weatherText.append("Condition : " + weather[0].currentCondition.getCondition());
                //condDescr.setText(weather.currentCondition.getCondition() + "(" + weather.currentCondition.getDescr() + ")");
                weatherText.append("\nTemper : " + Math.round((weather[0].temperature.getTemp() - 273.15)) + "C");
                //temp.setText("" + Math.round((weather.temperature.getTemp() - 273.15)) + "C");
                weatherText.append("\nHumidity : " + weather[0].currentCondition.getHumidity() + "%");
                //hum.setText("" + weather.currentCondition.getHumidity() + "%");
                weatherText.append("\nPressure : " + weather[0].currentCondition.getPressure() + " hPa");
                //press.setText("" + weather.currentCondition.getPressure() + " hPa");
                weatherText.append("\nWind speed : " + weather[0].wind.getSpeed() + " mps");
                //windSpeed.setText("" + weather.wind.getSpeed() + " mps");
                weatherText.append("\nWind direction : " + weather[0].wind.getDeg() + "");
                //windDeg.setText("" + weather.wind.getDeg() + "");
            tsagagaar.setText(weatherText.toString());
            }
            if(typer == 2){
                StringBuffer weatherText = new StringBuffer();
                //cityText.setText(weather[0].location.getCity() + "," + weather[0].location.getCountry());
                for (int i = 0; i < 8 ; i++){
                    weatherText.append("Condition : " + weather[i].currentCondition.getCondition());
                    //condDescr.setText(weather.currentCondition.getCondition() + "(" + weather.currentCondition.getDescr() + ")");
                    weatherText.append("\nTemper : " + Math.round((weather[i].temperature.getTemp() - 273.15)) + "C");
                    //temp.setText("" + Math.round((weather.temperature.getTemp() - 273.15)) + "C");
                    weatherText.append("\nHumidity : " + weather[i].currentCondition.getHumidity() + "%");
                    //hum.setText("" + weather.currentCondition.getHumidity() + "%");
                    weatherText.append("\nPressure : " + weather[i].currentCondition.getPressure() + " hPa");
                    //press.setText("" + weather.currentCondition.getPressure() + " hPa");
                    weatherText.append("\nWind speed : " + weather[i].wind.getSpeed() + " mps");
                    //windSpeed.setText("" + weather.wind.getSpeed() + " mps");
                    weatherText.append("\nWind direction : " + weather[i].wind.getDeg() + "");
                }
                tsagagaar.setText(weatherText.toString());
            }

        }
    }
}

package com.example.altantur.weatherapp;

import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import java.util.jar.Attributes;

/**
 * Created by altantur on 12/16/14.
 */
public class HandlingXMLStuff extends DefaultHandler{
    private XMLDataCollected info = new
            XMLDataCollected();
    public String getInformation(){
        return info.dataToString();
    }

    public void startElement(String uri, String localName, String qName,
                             Attributes attributes) throws SAXException {
        if (localName.equals("city")){
            String city = attributes.getValue("name");
            info.setCity(city);
        }else if (localName.equals("temperature ")){
            String t = attributes.getValue("value");
            int temp = Integer.parseInt(t);
            info.setTemp(temp);
        }
    }
}

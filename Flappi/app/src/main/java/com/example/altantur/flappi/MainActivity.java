package com.example.altantur.flappi;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;

public class MainActivity extends Activity {
    int w = 0, h= 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        //setContentView(R.layout.activity_mapper);
        DisplayMetrics metrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(metrics);
        h = metrics.heightPixels;
        w = metrics.widthPixels;
        View shapes = new SimpleDrawing(this);
        setContentView(shapes);
    }

    public class SimpleDrawing extends View {
        float width;
        float height;
        //Assets
        Bitmap background;
        Bitmap bird;
        Bitmap upper;
        Bitmap bottom;
        Bitmap bird_fall;
        Bitmap zam;

        Bitmap target;
        float zamx;
        float zamy;
        public SimpleDrawing(Context context){
            super(context);
            //Get display sizes
            DisplayMetrics metrics = new DisplayMetrics();
            getWindowManager().getDefaultDisplay().getMetrics(metrics);
            width = metrics.widthPixels;
            height = metrics.heightPixels;

            //Getting background and formatting to any screen
            Bitmap bmp = BitmapFactory.decodeResource(getResources(), R.drawable.bg);
            Matrix matrix=new Matrix();
            matrix.postScale(height / bmp.getWidth(),width/ bmp.getHeight());
            matrix.postRotate(90);
            background = Bitmap.createBitmap(bmp , 0, 0, bmp.getWidth(), bmp.getHeight(), matrix,  true);
            zam = BitmapFactory.decodeResource(getResources(), R.drawable.zam);
            bird = BitmapFactory.decodeResource(getResources(), R.drawable.bird_mini);
            upper = BitmapFactory.decodeResource(getResources(), R.drawable.u);
            bottom = BitmapFactory.decodeResource(getResources(), R.drawable.d);
            bird_fall = BitmapFactory.decodeResource(getResources(), R.drawable.bird_fall);
        }

        public  void run(){

        }
        public  void stop(){

        }


        @Override
        public boolean onTouchEvent(MotionEvent event) {
            float x = event.getX();
            float y = event.getY();
            switch (event.getAction()) {
                case MotionEvent.ACTION_DOWN:

                    break;
                case MotionEvent.ACTION_MOVE:
                    break;
                case MotionEvent.ACTION_UP:
                    break;
            }
            return true;
        }

        @Override
        protected void onDraw(Canvas canvas)
        {
            //Drawing image to Canvas
            canvas.drawBitmap(background, 0, 0, null);
            canvas.drawBitmap(zam, 0, 0, null);
            canvas.drawBitmap(bird, 0, 0, null);
            canvas.drawBitmap(bird_fall, 0, 200, null);
            canvas.drawBitmap(upper, 400, 10, null);
            canvas.drawBitmap(bottom, 700, 0, null);

        }

    }
}

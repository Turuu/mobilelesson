package com.example.second;

import android.R.menu;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class MainActivity extends ActionBarActivity {

	private EditText val1;
	private EditText val2;
	private EditText result;
	private Button add;
	private Button mul;
	private Button sub;
	private Button div;
	private static final int ADD_ID = Menu.FIRST;
	private static final int SUB_ID = Menu.FIRST + 1;
	private static final int MUL_ID = SUB_ID + 1;
	private static final int DIV_ID = MUL_ID + 1;

	@Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.lab2);
        val1 = (EditText)findViewById(R.id.etValueA);
        val2 = (EditText)findViewById(R.id.etValueB);
        result = (EditText)findViewById(R.id.etResult);
        add = (Button)findViewById(R.id.btAdd);
        mul = (Button)findViewById(R.id.btMulti);
        sub = (Button)findViewById(R.id.btSub);
        div = (Button)findViewById(R.id.btDivide);
        
        add.setOnClickListener(
                new View.OnClickListener()
                {
                    public void onClick(View view)
                    {
                    	result.setText(Integer.toString(Integer.parseInt(val1.getText().toString())
                    			+Integer.parseInt(val2.getText().toString())));
                        Log.i("add", result.getText().toString());
                    }
                });
        sub.setOnClickListener(
                new View.OnClickListener()
                {
                    public void onClick(View view)
                    {
                    	result.setText(Integer.toString(Integer.parseInt(val1.getText().toString())
                    			-Integer.parseInt(val2.getText().toString())));
                        Log.i("sub", result.getText().toString());
                    }
                });
        mul.setOnClickListener(
                new View.OnClickListener()
                {
                    public void onClick(View view)
                    {
                    	result.setText(Integer.toString(Integer.parseInt(val1.getText().toString())
                    			*Integer.parseInt(val2.getText().toString())));
                        Log.i("mul", result.getText().toString());
                    }
                });
        div.setOnClickListener(
                new View.OnClickListener()
                {
                    public void onClick(View view)
                    {
                    	if(Integer.parseInt(val2.getText().toString()) != 0)
                    	result.setText(Integer.toString(Integer.parseInt(val1.getText().toString())
                    			/Integer.parseInt(val2.getText().toString())));
                        Log.i("div", result.getText().toString());
                    }
                });
        
        
    }

	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		menu.add(0, ADD_ID, 0, R.string.menu_add);
		menu.add(0, SUB_ID, 0, R.string.menu_sub);
		menu.add(0, MUL_ID, 0, R.string.menu_mul);
		menu.add(0, DIV_ID, 0, R.string.menu_div);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		switch (id) {
		case ADD_ID:
			if(val1.getText() != null)
			result.setText(Integer.toString(Integer.parseInt(val1.getText().toString())
        			+Integer.parseInt(val2.getText().toString())));
            Log.i("Add", result.getText().toString());
			break;
		case SUB_ID:
			result.setText(Integer.toString(Integer.parseInt(val1.getText().toString())
        			-Integer.parseInt(val2.getText().toString())));
            Log.i("Sub", result.getText().toString());
			break;
		case MUL_ID:
			result.setText(Integer.toString(Integer.parseInt(val1.getText().toString())
        			*Integer.parseInt(val2.getText().toString())));
            Log.i("Multi", result.getText().toString());
			break;
		case DIV_ID:
			if(Integer.parseInt(val2.getText().toString()) != 0)
			result.setText(Integer.toString(Integer.parseInt(val1.getText().toString())
        			/Integer.parseInt(val2.getText().toString())));
            Log.i("Divide", result.getText().toString());
			break;

		default:
			break;
		}
		return super.onOptionsItemSelected(item);
	}

}

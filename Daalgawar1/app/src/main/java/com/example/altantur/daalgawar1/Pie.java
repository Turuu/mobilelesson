package com.example.altantur.daalgawar1;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.RectF;
import android.graphics.Typeface;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;


public class Pie extends ActionBarActivity {
    float per1 = 0, per2 = 0, per3 = 0, per4 = 0;
    float[] values;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Bundle personData = getIntent().getExtras();
        per1 = personData.getFloat("per1");
        per2 = personData.getFloat("per2");
        per3 = personData.getFloat("per3");
        per4 = personData.getFloat("per4");
        values = new  float[4];
        values[0] = per1;
        values[1] = per2;
        values[2] = per3;
        values[3] = per4;
        values = calPer(values);
        View shapes = new SimpleDrawing(this);
        setContentView(shapes);

    }

    private float[] calPer(float[] data) {
        float total=0;
        for(int i=0;i< 4;i++)
        {
            total+=data[i];
        }
        for(int i=0;i < 4 ;i++)
        {
            data[i]=360*(data[i]/total);
        }
        return data;
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_pie, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public class SimpleDrawing extends View {
        private Paint paint=new Paint();

        private float[] untsug;
        private int[] ungunuud ={Color.BLUE,Color.BLACK,Color.MAGENTA,Color.WHITE,Color.RED};
        RectF rectf = new RectF (10, 10, 400, 400);

        int temp=0;

        public SimpleDrawing(Context context){
            super(context);
            untsug = new float[values.length];
            for(int i=0;i<values.length;i++)
            {
                untsug[i]=values[i];
            }
        }


        @Override
        protected void onDraw(Canvas canvas)
        {
            super.onDraw(canvas);
            for (int i = 0; i < untsug.length; i++) {
                if (i == 0) {
                    paint.setColor(ungunuud[i]);
                    canvas.drawArc(rectf, 0, untsug[i], true, paint);
                }
                else
                {
                    temp += (int) untsug[i - 1];
                    paint.setColor(ungunuud[i]);
                    //canvas.drawText(values[i] + "", temp, untsug[i]);

                    canvas.drawArc(rectf, temp, untsug[i], true, paint);
                }
            }

        }



    }

}

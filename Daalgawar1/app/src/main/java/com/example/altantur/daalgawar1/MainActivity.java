package com.example.altantur.daalgawar1;

import android.content.Intent;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;


public class MainActivity extends ActionBarActivity {
    Button generate;
    EditText[] pers;
    int A1 = 123;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        pers = new EditText[4];

        pers[0] = (EditText) findViewById(R.id.editText);
        pers[1] = (EditText) findViewById(R.id.editText2);
        pers[2] = (EditText) findViewById(R.id.editText3);
        pers[3] = (EditText) findViewById(R.id.editText4);

        generate = (Button) findViewById(R.id.button);
        generate.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {

                float per1 = Integer.parseInt(pers[0].getText().toString());
                float per2 = Integer.parseInt(pers[1].getText().toString());
                float per3 = Integer.parseInt(pers[2].getText().toString());
                float per4 = Integer.parseInt(pers[3].getText().toString());

                Intent go = new Intent();
                go.putExtra("per1", per1);
                go.putExtra("per2", per2);
                go.putExtra("per3", per3);
                go.putExtra("per4", per4);
                go.setClassName("com.example.altantur.daalgawar1",
                        "com.example.altantur.daalgawar1.Pie");
                startActivityForResult(go, A1);
            }
        });

    }

    private float[] calculateData(float[] data) {
        float total=0;
        for(int i=0;i<data.length;i++)
        {
            total+=data[i];
        }
        for(int i=0;i<data.length;i++)
        {
            data[i]=360*(data[i]/total);
        }
        return data;
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}

package com.example.fifth;


import android.support.v7.app.ActionBarActivity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.res.Resources;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TabHost;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.TabHost.TabSpec;

public class MainActivity extends ActionBarActivity implements OnClickListener {

	private Button[] numbers = new Button[10];
	private Button[] signs = new Button[5];
	private Button dot;
	private Button clear;
	private Button clearhistory;
	private TextView disp;
	private double operand1, operand2;
	private boolean flag = true;
	private String uildel;
	private StringBuffer history = new StringBuffer();
	private TextView ustgah;
	private static final int EXIT = Menu.FIRST;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		disp = (TextView) findViewById(R.id.disp);
		numbers[0] = (Button) findViewById(R.id.t0);
		numbers[1] = (Button) findViewById(R.id.t1);
		numbers[2] = (Button) findViewById(R.id.t2);
		numbers[3] = (Button) findViewById(R.id.t3);
		numbers[4] = (Button) findViewById(R.id.t4);
		numbers[5] = (Button) findViewById(R.id.t5);
		numbers[6] = (Button) findViewById(R.id.t6);
		numbers[7] = (Button) findViewById(R.id.t7);
		numbers[8] = (Button) findViewById(R.id.t8);
		numbers[9] = (Button) findViewById(R.id.t9);
		signs[0] = (Button) findViewById(R.id.tentsuu);
		signs[1] = (Button) findViewById(R.id.nemeh);
		signs[2] = (Button) findViewById(R.id.hasah);
		signs[3] = (Button) findViewById(R.id.urjih);
		signs[4] = (Button) findViewById(R.id.huwaah);
		dot = (Button) findViewById(R.id.tseg);
		clear = (Button) findViewById(R.id.tsewerleh);
		clearhistory = (Button) findViewById(R.id.clear);
		ustgah = (TextView) findViewById(R.id.history);
		initializeUI();
		for (int i = 0; i < 10; i++) {
			numbers[i].setOnClickListener(this);
		}
		for (int i = 0; i < 5; i++) {
			signs[i].setOnClickListener(this);
		}
		dot.setOnClickListener(this);
		clear.setOnClickListener(this);
		clearhistory.setOnClickListener(this);

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		menu.add(0, EXIT, 0, R.string.clear);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		switch (id) {
		case EXIT:
			Dialog exit = construcconfirmexitDialog();
			exit.show();
			break;
		}
		return super.onOptionsItemSelected(item);
	}

	public void initializeUI() {
		TabSpec calc, logger;
		Resources res = getResources();
		TabHost tabHost = (TabHost) findViewById(R.id.tab_host);
		tabHost.setup();
		calc = tabHost.newTabSpec("calc").setContent(R.id.tableLayout1)
				.setIndicator("Calculator");
		logger = tabHost.newTabSpec("log").setContent(R.id.tableLayout2)
				.setIndicator("Log");
		tabHost.addTab(calc);
		tabHost.addTab(logger);
		tabHost.setCurrentTab(0);
	}

	@Override
	public void onClick(View v) {
		String text = "";
		if (flag)
			text = disp.getText().toString();
		else
			flag = true;
		switch (v.getId()) {
		case R.id.t0:
			text += "0";
			break;
		case R.id.t1:
			text += "1";
			break;
		case R.id.t2:
			text += "2";
			break;
		case R.id.t3:
			text += "3";
			break;
		case R.id.t4:
			text += "4";
			break;
		case R.id.t5:
			text += "5";
			break;
		case R.id.t6:
			text += "6";
			break;
		case R.id.t7:
			text += "7";
			break;
		case R.id.t8:
			text += "8";
			break;
		case R.id.t9:
			text += "9";
			break;
		case R.id.tseg:
			text += ".";
			break;
		case R.id.nemeh:
			operand1 = Double.parseDouble(text);
			uildel = "add";
			flag = false;
			break;
		case R.id.hasah:
			operand1 = Double.parseDouble(text);
			uildel = "sub";
			flag = false;
			break;
		case R.id.urjih:
			operand1 = Double.parseDouble(text);
			uildel = "mul";
			flag = false;
			break;
		case R.id.huwaah:
			operand1 = Double.parseDouble(text);
			uildel = "div";
			flag = false;
			break;
		case R.id.tentsuu:
			operand2 = Double.parseDouble(text);
			text = doSign();
			break;
		case R.id.tsewerleh:
			text = "";
			operand1 = 0;
			operand2 = 0;
			flag = true;
			break;
		case R.id.clear:
			AlertDialog.Builder builder = new AlertDialog.Builder(this);
			builder.setTitle("Delete?")
					.setMessage("Are you sure to clear now?")
					.setCancelable(true)
					.setPositiveButton("Yes",
							new DialogInterface.OnClickListener() {
								@Override
								public void onClick(DialogInterface dialog,
										int which) {
									history = new StringBuffer();
									ustgah.setText("");
								}
							})
					.setNegativeButton("No",
							new DialogInterface.OnClickListener() {
								@Override
								public void onClick(DialogInterface dialog,
										int which) {
									dialog.cancel();
								}
							});
			AlertDialog dialog = builder.create();
			dialog.show();
			break;
		}
		disp.setText(text);
	}

	private Dialog construcconfirmexitDialog() {
		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		builder.setTitle("Exit?")
				.setMessage("Are you sure to exit now?")
				.setCancelable(false)
				.setPositiveButton("Yes",
						new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog,
									int which) {
								finish();
							}
						})
				.setNegativeButton("No", new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int which) {
						dialog.cancel();
					}
				});
		AlertDialog alert = builder.create();
		return alert;
	}

	public String doSign() {
		Double result = 0.0;
		String temp = "";
		if (operand1 != 0 && operand2 != 0)
			switch (uildel) {
			case "add":
				result = operand1 + operand2;
				temp = " " + operand1 + " + " + operand2 + " = " + result;
				break;
			case "sub":
				result = operand1 - operand2;
				temp = " " + operand1 + " - " + operand2 + " = " + result;
				break;
			case "mul":
				result = operand1 * operand2;
				temp = " " + operand1 + " * " + operand2 + " = " + result;
				break;
			case "div":
				result = operand1 / operand2;
				temp = " " + operand1 + " / " + operand2 + " = " + result;
				break;
			default:
				break;
			}
		temp += "\n";
		history.append(temp);
		ustgah.setText(history);
		operand1 = result;

		return String.valueOf(result);
	}
}

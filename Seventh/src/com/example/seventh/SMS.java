package com.example.seventh;

public class SMS {
	private String recNumber;
	private String body;
	private String time;
	
	public SMS(String recNumber, String body, String time) {
		this.recNumber = recNumber;
		this.body = body;
		this.time = time;
	}
	public String getRecNumber() {
		return recNumber;
	}
	public void setRecNumber(String recNumber) {
		this.recNumber = recNumber;
	}
	public String getBody() {
		return body;
	}
	public void setBody(String body) {
		this.body = body;
	}
	public String getTime() {
		return time;
	}
	public void setTime(String time) {
		this.time = time;
	}
	
	
	
}

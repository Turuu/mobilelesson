package com.example.seventh;

import android.annotation.SuppressLint;
import android.util.Log;

import java.text.SimpleDateFormat;
import java.util.Calendar;

public class Time {
	public static final String DATE_FORMAT_NOW = "yyyy-MM-dd HH:mm:ss";
	@SuppressLint("SimpleDateFormat")
	public static String now() {
		Calendar cal = Calendar.getInstance();
		SimpleDateFormat sdf = new SimpleDateFormat(DATE_FORMAT_NOW);
		Log.i("Minute", "" + Calendar.MINUTE);
		return sdf.format(cal.getTime());
	}
	
	public static int minute(){
		Calendar now = Calendar.getInstance();
		int minute = now.get(Calendar.MINUTE);
		return minute;
	}
}

package com.example.seventh;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.telephony.SmsManager;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class compSMS extends Activity {
	private Button send;
	private Button cancel;
	private EditText dist;
	private EditText contentBody;
	private Context context;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.comp);
		context = getApplicationContext();
		send = (Button) findViewById(R.id.btSend);
		cancel = (Button) findViewById(R.id.btCancel);
		send.setOnClickListener(new View.OnClickListener() {

			public void onClick(View v) {
				dist = (EditText) findViewById(R.id.toNumber);
				contentBody = (EditText) findViewById(R.id.contentBody);
				try{
				SmsManager m = SmsManager.getDefault();
				m.sendTextMessage(dist.getText().toString(), null, contentBody
						.getText().toString(), null, null);
				Message.message(context, "SMS Sent");
				} catch(Exception e){
					Message.message(context, "Failed to send!");
				}
				finish();
			}
		});
		cancel.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				finish();
			}
		});
	}

}

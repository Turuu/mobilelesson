package com.example.seventh;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.List;

import android.app.ListActivity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.BatteryManager;
import android.os.Bundle;
import android.telephony.CellInfo;
import android.telephony.CellLocation;
import android.telephony.PhoneStateListener;
import android.telephony.ServiceState;
import android.telephony.TelephonyManager;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends ListActivity {
	private static final int A1 = 123;
	private Button comp;
	private Button sim;
	private String[] msgs;
	private static Context context;
	IntentFilter filter = new IntentFilter();
	private BroadcastReceiver receiver;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {

		super.onCreate(savedInstanceState);
		MainActivity.context = getApplicationContext();
		filter.addAction(Intent.ACTION_TIME_TICK);
		filter.addAction(Intent.ACTION_POWER_CONNECTED);
		filter.addAction(Intent.ACTION_POWER_DISCONNECTED);
		filter.addAction(Intent.ACTION_BOOT_COMPLETED);

		receiver = new BroadcastReceiver() {
			@Override
			public void onReceive(Context context, Intent intent) {
				String action = intent.getAction();
				if (action.equals(Intent.ACTION_TIME_TICK))
					if (Time.minute() % 5 == 0)
						writeFile("deviceInfo.txt", "Time: " + Time.now()
								+ "\n");
					else if (Time.minute() == 0)
						writeFile("deviceInfo.txt",
								"TimeChanged: " + Time.minute() + "\n");
				if (action.equals("android.intent.action.BOOT_COMPLETED"))
					writeFile("deviceInfo.txt", "BootCompleted: " + Time.now()
							+ "\n");
				else if (action.equals(Intent.ACTION_POWER_CONNECTED))
					writeFile("deviceInfo.txt", "PowerConnected: " + Time.now()
							+ "\n");
				else if (action.equals(Intent.ACTION_POWER_DISCONNECTED))
					writeFile("deviceInfo.txt",
							"PowerDisconnected: " + Time.now() + "\n"
									+ "Battery Level:"
									+ BatteryManager.EXTRA_LEVEL + "\n");
				Message.message(context, readFile("deviceInfo.txt"));
			}
		};
		registerReceiver(receiver, filter);

		String tur = readFile("Messages2.txt");
		if (tur == null)
			tur = "";
		Bundle extras = getIntent().getExtras();
		setContentView(R.layout.activity_main);
		comp = (Button) findViewById(R.id.composeSMS);
		sim = (Button) findViewById(R.id.sim);
		comp.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				Intent compose = new Intent();
				compose.setClassName("com.example.seventh",
						"com.example.seventh.compSMS");
				startActivityForResult(compose, A1);
			}
		});
		String prev = "";
		if (extras != null) {
			prev += extras.getString("sender") + "\n";
			prev += extras.getString("msjContent") + "\n";
			prev += extras.getString("time") + "\n\n";
			tur += prev;
		}
		sim.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				// For testing purpose
				Message.message(context, "Huuue: " + Time.minute());
				writeSimInfo();
			}
		});
		if (!prev.equals(""))
			writeFile("Messages2.txt", prev);
		msgs = tur.split("\n\n");
		setListAdapter(new ArrayAdapter<String>(this, R.layout.simplelist, msgs));
		ListView listView = getListView();
		listView.setTextFilterEnabled(true);
		listView.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				Toast.makeText(getApplicationContext(),
						((TextView) view).getText(), Toast.LENGTH_SHORT).show();
			}
		});
	}

	public static Context getAppContext() {
		return MainActivity.context;
	}

	private String readFile(String filename) {
		String fileContents;
		try {
			InputStream file = new BufferedInputStream(openFileInput(filename));
			byte[] data = new byte[file.available()];
			file.read(data, 0, file.available());
			fileContents = new String(data);
			file.close();
		} catch (Exception noFile) {
			fileContents = null;
		}
		return fileContents;
	}

	public void writeFile(String filename, String fileContents) {
		try {
			OutputStream file = new BufferedOutputStream(openFileOutput(
					filename, MODE_APPEND | MODE_PRIVATE));
			file.write(fileContents.getBytes());
			file.close();
		} catch (Exception noFile) {
		}
	}

	public static void write(String filename, Context c, String string)
			throws IOException {
		try {
			FileOutputStream fos = c.openFileOutput(filename,
					Context.MODE_PRIVATE);
			fos.write(string.getBytes());
			fos.close();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	private void writeSimInfo() {
		TelephonyManager mTelephonyMgr = (TelephonyManager) getSystemService(TELEPHONY_SERVICE);
		String country = mTelephonyMgr.getSimCountryIso();
		String SimOperator = mTelephonyMgr.getSimOperatorName();
		String SimSerialNumber = mTelephonyMgr.getSimSerialNumber();
		String SubscriberId = mTelephonyMgr.getSubscriberId();
		String mob = mTelephonyMgr.getLine1Number();
		writeFile("SIMInfo.txt", country + "\n" + SimOperator + "\n"
				+ SimSerialNumber + "\n" + SubscriberId + "\n" + mob);
	}

}

package com.example.seventh;

import java.io.BufferedWriter;
import java.io.File;

import java.io.FileWriter;
import java.io.IOException;

import java.util.List;

import android.content.Context;
import android.os.Environment;
import android.telephony.CellInfo;
import android.telephony.CellLocation;
import android.telephony.PhoneStateListener;
import android.telephony.ServiceState;
import android.telephony.TelephonyManager;

public class myPhoneListener extends PhoneStateListener {
	String stateString = "N/A";
	Context context;
	boolean callEnded = false;

	public void onCallStateChanged(int state, String incomingNumber) {
		switch (state) {
		case TelephonyManager.CALL_STATE_IDLE:
			writeFile("phonestate.txt", "LISTEN_CALL_STATE: " + Time.now()
					+ " - " + "Idle");
			break;
		case TelephonyManager.CALL_STATE_OFFHOOK:
			writeFile("phonestate.txt", "LISTEN_CALL_STATE: " + Time.now()
					+ " - " + "Offhook");
			callEnded = true;
			break;
		case TelephonyManager.CALL_STATE_RINGING:
			writeFile("phonestate.txt", "LISTEN_CALL_STATE: " + Time.now()
					+ " - " + "Ringing");
			break;

		default:
			break;
		}

	}

	public void onCallForwardingIndicatorChanged(boolean cfi) {
		if (cfi)
			stateString = "Call Forwarding Indicator Changed";
		else
			stateString = "Call Forwarding Indicator Didnt Change";
		writeFile("phonestate.txt", Time.now() + " - " + stateString + "\n");
	}

	public void onCellInfoChanged(List<CellInfo> cellInfo) {
		writeFile("phonestate.txt", Time.now() + " - " + "Cell Info changed"
				+ "\n");
	}

	public void onCellLocationChanged(CellLocation location) {
		writeFile("phonestate.txt", Time.now() + " - "
				+ "Cell location changed" + "\n");
	}

	public void onDataActivity(int direction) {
		switch (direction) {
		case TelephonyManager.DATA_ACTIVITY_NONE:
			stateString = "None";
			break;
		case TelephonyManager.DATA_ACTIVITY_IN:
			stateString = "In";
			break;
		case TelephonyManager.DATA_ACTIVITY_OUT:
			stateString = "Out";
			break;
		case TelephonyManager.DATA_ACTIVITY_INOUT:
			stateString = "InOut";
			break;
		case TelephonyManager.DATA_ACTIVITY_DORMANT:
			stateString = "Dormant";
			break;
		}
		writeFile("phonestate.txt", Time.now() + " - " + stateString + "\n");
	}

	public void onServiceStateChanged(ServiceState serviceState) {
		switch (serviceState.getState()) {
		case ServiceState.STATE_EMERGENCY_ONLY:
			stateString = "Emergency only";
			break;
		case ServiceState.STATE_IN_SERVICE:
			stateString = "In service";
			break;
		case ServiceState.STATE_OUT_OF_SERVICE:
			stateString = "Out of service";
			break;
		case ServiceState.STATE_POWER_OFF:
			stateString = "Power off";
			break;
		}
		writeFile("phonestate.txt", Time.now() + " - " + stateString + "\n");
	}

	public void writeFile(String filename, String text) {
		File sdDir = Environment.getExternalStorageDirectory();
		File file = new File(sdDir, filename);

		try {
			// BufferedWriter for performance, true to set append to file flag
			BufferedWriter buf = new BufferedWriter(new FileWriter(file, true));
			buf.append(text);
			buf.newLine();
			buf.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}

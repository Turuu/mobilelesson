package com.example.altantur.daalgawar2;

import android.content.Intent;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import java.util.Date;
import java.util.logging.Handler;


public class MainActivity extends ActionBarActivity {
    private static final int MACT1 = Menu.FIRST;
    private static final int MACT2 = Menu.FIRST + 1;
    TextView disp;
    Button startop;
    Button relap;
    Date now;
    String time1;
    String time2;
    int min;
    int sec;
    int doli;
    boolean isStart = false;
    //private Handler mHandler;
    private boolean mStarted;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
       // mHandler = new Handler();

        startop = (Button) findViewById(R.id.button);
        relap = (Button) findViewById(R.id.button2);
        disp = (TextView) findViewById(R.id.textView);
        startop.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if(isStart){
                    startop.setText("Start");
                    relap.setText("Reset");
                    isStart = false;
                    disp.setText("00:00.0");
                }
                else {
                    startop.setText("Stop");
                    relap.setText("Lap");
                    isStart = true;
                    mRun.run();
                }
            }
        });
        relap.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent go = new Intent();
                go.setClassName("com.example.altantur.daalgawar2",
                        "com.example.altantur.daalgawar2.TimerActivity");
                startActivityForResult(go, 134);
            }
        });

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        menu.add(0, MACT1, 0, R.string.act1);
        menu.add(0, MACT2, 0, R.string.act2);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }


    private Runnable mRun = new Runnable() {
        @Override
        public void run() {
            if (mStarted) {
                long seconds = (System.currentTimeMillis()) / 1000;
                disp.setText(String.format("%02d:%02d", seconds / 60, seconds % 60));
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }

            }
        }

    };

}

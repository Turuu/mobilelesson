package com.example.altantur.daalgawar2;

import java.util.Timer;
import java.util.TimerTask;
import android.os.Bundle;
import android.os.Handler;
import android.app.Activity;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.example.altantur.daalgawar2.R;

public class TimerActivity extends ActionBarActivity {
    private static final int MACT1 = Menu.FIRST;
    private static final int MACT2 = Menu.FIRST + 1;
    TimerTask timerTask;
    int totalSecond=0;
    int sec;
    int min;
    Button startop;
    Button relap;
    Button viewIt;
    EditText results;
    boolean tStarted = false;
    TextView disp;
    TextView lap;
    private DatabaseHelper nameHelper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_timer);
        startop = (Button) findViewById(R.id.button);
        relap = (Button) findViewById(R.id.button2);
        disp  = (TextView) findViewById(R.id.textView);
        lap = (TextView) findViewById(R.id.lap);
        nameHelper = new DatabaseHelper(this);
        viewIt = (Button) findViewById(R.id.buttonResult);
        results = (EditText) findViewById(R.id.result);
        results.setHint("Enter result number");
        results.setVisibility(View.INVISIBLE);
        viewIt.setVisibility(View.INVISIBLE);

        startop.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if(!tStarted){
                    startTimer(v);
                    tStarted = true;
                    startop.setText("Stop");
                    relap.setText("Lap");
                }
                else  {
                    stopTimer(v);
                    tStarted = false;
                    startop.setText("Start");
                    relap.setText("Reset");
                }
            }
        });
        relap.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (!tStarted) {
                    disp.setText("0:0");
                } else {
                    String laps = "";
                    laps = lap.getText().toString();
                    laps = laps + "\n" + disp.getText().toString();
                    lap.setText(laps);
                }
            }
        });
        viewIt.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                String lapsers = nameHelper.fetchUser(Integer.parseInt(results.getText().toString()));
                lap.setText(lapsers);
            }
        });
    }


    public void startTimer(View view) {
        final Handler handler = new Handler();
        Timer timer = new Timer();
        timerTask = new TimerTask() {
            public void run() {
                handler.post(new Runnable() {
                    public void run() {
                        TextView timer = (TextView)findViewById(R.id.textView);
                        min = totalSecond / 60;
                        sec = totalSecond % 60;
                        timer.setText(min + ":" + sec);
                        totalSecond ++;
                    }
                });
            }};

        timer.schedule(timerTask, 0, 1000);
    }


    public void stopTimer(View view) {
        timerTask.cancel();
        timerTask = null;
        totalSecond = 0;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        menu.add(0, MACT1, 0, R.string.act1);
        menu.add(0, MACT2, 0, R.string.act2);

        return true;
    }

    private  String huwirga1(String data){
        return data.replace("\n", "H");
    }

    private  String huwirga2(String data){
        return data.replace("H", "\n");
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        switch (id) {
            case MACT1:
                lap.setText(nameHelper.fetchID());
                results.setVisibility(View.VISIBLE);
                viewIt.setVisibility(View.VISIBLE);

                break;
            case MACT2:
                nameHelper.addUser(lap.getText().toString());
                break;
            default:
                break;
        }
        return super.onOptionsItemSelected(item);
    }


}
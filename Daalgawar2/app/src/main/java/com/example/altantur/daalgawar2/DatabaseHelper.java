package com.example.altantur.daalgawar2;

/**
 * Created by altantur on 1/5/15.
 */

 import android.content.Context;
        import android.database.Cursor;
        import android.database.sqlite.SQLiteDatabase;
        import android.database.sqlite.SQLiteOpenHelper;
 import android.util.Log;

public class DatabaseHelper extends SQLiteOpenHelper {

    private static final String DATABASE_NAME = "info.db";
    private static final int DATABASE_VERSION = 1;
    private Context context;

    public DatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        this.context = context;
        //Message.message(context, "Constructor called");
    }

    public void onCreate(SQLiteDatabase nameDb) {
        nameDb.execSQL("CREATE TABLE " + "USER" + " (" + "_ID"
                + " INTEGER PRIMARY KEY AUTOINCREMENT," +  "TIMES" + " VARCHAR(255));");
        // Initial user recording

    }

    public void onUpgrade(SQLiteDatabase arg0, int oldVersion, int newVersion) {

    }



    public String fetchUser(int id) {
        String query = "SELECT * FROM USER WHERE _ID = " + id
                + ";";
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor result;
        result = db.rawQuery(query, null);
        String temp = null;
        if (result != null && result.moveToFirst()) {
            temp = result.getString(result.getColumnIndex("TIMES"));
        }
        return temp;
    }
    public String fetchID() {
        String query = "SELECT _ID FROM USER;";
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor result;
        result = db.rawQuery(query, null);
        String temp = "";
        if (result != null && result.moveToFirst()) {
            do
            temp = temp + "\nresult" + result.getString(result.getColumnIndex("_ID"));
            while(result.moveToNext());
        }
        return temp;
    }


    public boolean addUser(String times) {
        try {
            Log.i("Times", times);
            String query = "INSERT INTO  USER (TIMES) VALUES('" + times +  "');";
            SQLiteDatabase db = this.getWritableDatabase();
            db.execSQL(query);
            return true;
        } catch (Exception e) {
            return false;
        }

    }




}

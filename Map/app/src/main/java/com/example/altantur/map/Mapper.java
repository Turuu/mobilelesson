package com.example.altantur.map;

import android.app.Activity;
import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Point;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Message;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Display;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Toast;

import java.util.logging.Handler;


public class Mapper extends Activity {
    int w = 0, h= 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_mapper);
        DisplayMetrics metrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(metrics);
        h = metrics.heightPixels;
        w = metrics.widthPixels;
        View shapes = new SimpleDrawing(this);
        setContentView(shapes);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_mapper, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public class SimpleDrawing extends View {
        float width;
        float height;
        float zamX;
        float zamY;
        float gapX;
        float gapY;
        Bitmap map;
        Bitmap nuclear;
        float targetX;
        float targetY;
        public SimpleDrawing(Context context){
            super(context);
            //Get display sizes
            DisplayMetrics metrics = new DisplayMetrics();
            getWindowManager().getDefaultDisplay().getMetrics(metrics);
            width = metrics.widthPixels;
            height = metrics.heightPixels;
            //Get picture and formatting to any screen
            Bitmap bmp = BitmapFactory.decodeResource(getResources(), R.drawable.bg);
            Matrix matrix=new Matrix();
            matrix.postScale(height / bmp.getWidth(),width/ bmp.getHeight());
            matrix.postRotate(90);
            map = Bitmap.createBitmap(bmp , 0, 0, bmp.getWidth(), bmp.getHeight(), matrix,  true);
            //Get hero and formatting to path
            Bitmap target = BitmapFactory.decodeResource(getResources(), R.drawable.zam);

            nuclear = Bitmap.createBitmap(target, 0, 0, target.getWidth(), target.getHeight());

        }

        @Override
        public boolean onTouchEvent(MotionEvent event) {
            float x = event.getX();
            float y = event.getY();
            switch (event.getAction()) {
                case MotionEvent.ACTION_DOWN:
                    break;
                case MotionEvent.ACTION_MOVE:
                    break;
                case MotionEvent.ACTION_UP:
                    break;
            }
            return true;
        }



        @Override
        protected void onDraw(Canvas canvas)
        {
            //Drawing image to Canvas
            canvas.drawBitmap(map, 0, 0, null);
            canvas.drawBitmap(nuclear, width, height , null);
        }



    }




}

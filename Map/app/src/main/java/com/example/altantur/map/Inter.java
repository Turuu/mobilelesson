package com.example.altantur.map;

import android.app.Activity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageView;


public class Inter extends Activity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.inter);
        final Animation animCycle = AnimationUtils.loadAnimation(this, R.anim.cycle);
        final Animation animAccelerate = AnimationUtils.loadAnimation(this, R.anim.accelerate);
        final Animation animBounce = AnimationUtils.loadAnimation(this, R.anim.bounce);
        final ImageView image = (ImageView)findViewById(R.id.image);
        Button btnCycle = (Button)findViewById(R.id.cycle);
        Button btnAccelerate = (Button)findViewById(R.id.accelerate);
        Button btnBounce = (Button)findViewById(R.id.bounce);
        btnCycle.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                image.startAnimation(animCycle);
            }
        });
        btnAccelerate.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                image.startAnimation(animAccelerate);
            }
        });
        btnBounce.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                image.startAnimation(animBounce);
            }
        });
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_inter, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}

package com.example.fourth;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;

public class SignUp extends Activity {
	private TextView name;
	private EditText pass;
	private EditText age;
	private EditText sex;
	private EditText phone;
	private DatePicker dob;
	private DatabaseHelper nameHelper;
	private Button submit;
	private Button cancel;
	private Context context;
	private User current;

	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.sup);
		name = (TextView) findViewById(R.id.textView1);
		Intent data = getIntent();
		name.setText(data.getStringExtra("name"));
		pass = (EditText) findViewById(R.id.Passr);
		final String prepass = data.getStringExtra("pass");
		age = (EditText) findViewById(R.id.Nas);
		sex = (EditText) findViewById(R.id.Huis);
		phone = (EditText) findViewById(R.id.Utas);
		submit = (Button) findViewById(R.id.btSub);
		cancel = (Button) findViewById(R.id.btCan);
		dob = (DatePicker) findViewById(R.id.onSar);
		context = getApplicationContext();
		nameHelper = new DatabaseHelper(this);
		submit.setOnClickListener(new View.OnClickListener() {

			public void onClick(View v) {
				if (name.getText().toString() != null
						&& age.getText().toString() != null
						&& sex.getText().toString() != null
						&& phone.getText().toString() != null
						&& pass.getText().toString() != null) {
					if (pass.getText().toString().equals(prepass)) {
						current = new User(name.getText().toString(),
								hashit.md5(prepass), age.getText().toString(),
								sex.getText().toString(), phone.getText()
										.toString(), dob.getYear(), dob
										.getMonth(), dob.getDayOfMonth());
						if(nameHelper.addUser(current)){
							Message.message(context, "Succesfully registered");
							finish();
						}
					} else
						Message.message(context, "Password didnt match!");
				} else
					Message.message(context, "Fill all fields!");
			}
		});

		cancel.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				finish();
			}
		});

	}
}

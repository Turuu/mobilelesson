package com.example.fourth;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.InputStream;
import java.io.OutputStream;

import android.support.v7.app.ActionBarActivity;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends ActionBarActivity {

	EditText name;
	EditText pass;
	Button login;
	Button sup;
	Button contacts;
	private DatabaseHelper namesHelper;
	private static final int A1 = 123, A2 = 124, A3 = 125;
	private Context context;
	String URL = "content://com.example.provider.fourth/user";
	Uri user = Uri.parse(URL);

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.lab4);
		login = (Button) findViewById(R.id.btLogin);
		sup = (Button) findViewById(R.id.btSup);
		contacts = (Button) findViewById(R.id.contacts);
		name = (EditText) findViewById(R.id.name);
		pass = (EditText) findViewById(R.id.pass);
		namesHelper = new DatabaseHelper(this);
		context = getApplicationContext();
		String prename = readFile();
		if (prename != null)
			name.setText(prename);

		contacts.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				Intent go = new Intent();
				go.setClassName("com.example.fourth",
						"com.example.fourth.SezamContacts");
				startActivityForResult(go, A3);
			}
		});
		login.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				String tname = name.getText().toString();
				String tpass = hashit.md5(pass.getText().toString().trim());
				ContentResolver cr = getContentResolver();
				String[] projection = {"NAME", "PASS"};
				Cursor c = cr.query(user, projection, null, null, null);
				boolean flag = true;
				if (c.moveToFirst()) {
					do {
						if (c.getString(0).equals(tname)
								&& c.getString(1).equals(tpass)) {
							writeFile(name.getText().toString());
							Intent go = new Intent();
							go.putExtra("name", name.getText().toString());
							go.setClassName("com.example.fourth",
									"com.example.fourth.UserInfo");
							startActivityForResult(go, A2);
							flag = false;
						}
					} while (c.moveToNext());
				}
				if (flag)
					Toast.makeText(getApplicationContext(),
							"Your name or password is incorrect!",
							Toast.LENGTH_LONG).show();
			}
		});
		sup.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				String username = name.getText().toString();
				String password = pass.getText().toString();
				if (namesHelper.fetchUser(username) != null)
					Message.message(context, "Username already registered");
				else {
					Intent go = new Intent();
					go.putExtra("name", username);
					go.putExtra("pass", password);
					go.setClassName("com.example.fourth",
							"com.example.fourth.SignUp");
					startActivityForResult(go, A1);
				}
			}
		});
	}

	private String readFile() {
		String fileContents;
		try {
			InputStream file = new BufferedInputStream(openFileInput("name"));
			byte[] data = new byte[file.available()];
			file.read(data, 0, file.available());
			fileContents = new String(data);
			file.close();
		} catch (Exception noFile) {
			fileContents = null;
		}
		return fileContents;
	}

	private void writeFile(String fileContents) {
		try {
			OutputStream file = new BufferedOutputStream(openFileOutput("name",
					MODE_PRIVATE));
			file.write(fileContents.getBytes());
			file.close();
		} catch (Exception noFile) {
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
}

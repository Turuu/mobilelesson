package com.example.fourth;

import android.app.Activity;
import android.content.ContentResolver;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;

public class UserInfo extends Activity {
	private TextView name;
	private EditText age;
	private EditText sex;
	private EditText phone;
	private DatePicker dob;
	private DatabaseHelper nameHelper;
	private Button save;
	private Button changePass;
	private User selected = null;
	private int A1 = 1221;

	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.usr);
		save = (Button) findViewById(R.id.btSave);
		changePass = (Button) findViewById(R.id.btCan);
		Intent data = getIntent();
		String ner = data.getStringExtra("name");
		nameHelper = new DatabaseHelper(this);
		// selected = nameHelper.fetchUser(ner);

		String URL = "content://com.example.provider.fourth/user";
		Uri user = Uri.parse(URL);
		ContentResolver cr = getContentResolver();
		Cursor result = cr
				.query(user, null, "name = '" + ner + "'", null, null);
		if (result.moveToFirst()) {
			
			String[] onSar = result.getString(result.getColumnIndex("DOB"))
					.split("-");
			selected = new User(ner, result.getString(result
					.getColumnIndex("PASS")), result.getString(result
					.getColumnIndex("AGE")), result.getString(result
					.getColumnIndex("SEX")), result.getString(result
					.getColumnIndex("PHONUMBER")), Integer.parseInt(onSar[0]),
					Integer.parseInt(onSar[1]), Integer.parseInt(onSar[2]));
		}

		if (selected != null) {
			name = (TextView) findViewById(R.id.name);
			age = (EditText) findViewById(R.id.Nas);
			sex = (EditText) findViewById(R.id.Huis);
			phone = (EditText) findViewById(R.id.Utas);
			dob = (DatePicker) findViewById(R.id.onSar);
			name.setText(ner);
			age.setText(selected.getAge());
			sex.setText(selected.getSex());
			phone.setText(selected.getPhone());
			dob.init(selected.getYear(), selected.getMonth(),
					selected.getDay(), null);
		}

		save.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				if (selected != null) {
					selected.setAge(age.getText().toString());
					selected.setSex(sex.getText().toString());
					selected.setPhone(phone.getText().toString());
					selected.setYear(dob.getYear());
					selected.setMonth(dob.getMonth());
					selected.setDay(dob.getDayOfMonth());
					nameHelper.updateUser(selected);
				}
			}
		});

		changePass.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				Intent go = new Intent();
				go.putExtra("name", name.getText());
				go.putExtra("pass", selected.getPass());
				go.setClassName("com.example.fourth",
						"com.example.fourth.changePassword");
				startActivityForResult(go, A1);
			}
		});

	}
}

package com.example.fourth;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class DatabaseHelper extends SQLiteOpenHelper {

	private static final String DATABASE_NAME = "info.db";
	private static final int DATABASE_VERSION = 1;
	private Context context;
	
	public DatabaseHelper(Context context) {
		super(context, DATABASE_NAME, null, DATABASE_VERSION);
		this.context = context;
		//Message.message(context, "Constructor called");
	}

	public void onCreate(SQLiteDatabase nameDb) {
		nameDb.execSQL("CREATE TABLE " + "USER" + " (" + "_ID"
				+ " INTEGER PRIMARY KEY AUTOINCREMENT," +  "TIMES" + " VARCHAR(255));");
		// Initial user recording
		
	}

	public void onUpgrade(SQLiteDatabase arg0, int oldVersion, int newVersion) {

	}

	

	public User fetchUser(String name) {
		String query = "SELECT * FROM USER WHERE NAME = '" + name
				+ "' COLLATE NOCASE;";
		SQLiteDatabase db = this.getWritableDatabase();
		Cursor result;
		result = db.rawQuery(query, null);
		User temp = null;
		if (result != null && result.moveToFirst()) {
			String[] onSar = result.getString(result.getColumnIndex("DOB"))
					.split("-");
			temp = new User(name, result.getString(result
					.getColumnIndex("PASS")), result.getString(result
					.getColumnIndex("AGE")), result.getString(result
					.getColumnIndex("SEX")), result.getString(result
					.getColumnIndex("PHONUMBER")), Integer.parseInt(onSar[0]),
					Integer.parseInt(onSar[1]), Integer.parseInt(onSar[2]));
		}
		return temp;
	}

	public boolean addUser(String times) {
		try {
			String query = "INSERT INTO  USER (TIMES) VALUES("
					+times+
					"')";
			SQLiteDatabase db = this.getWritableDatabase();
			db.execSQL(query);
			return true;
		} catch (Exception e) {
			return false;
		}

	}


	

}

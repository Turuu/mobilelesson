package com.example.fourth;


import java.util.ArrayList;

import android.app.ListActivity;
import android.content.ContentResolver;
import android.database.Cursor;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.widget.ArrayAdapter;
import android.widget.ListView;

public class SezamContacts extends ListActivity {
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		//setContentView(R.layout.contacts);
		//Fetching contacts names
		ContentResolver cr = getContentResolver();
		Cursor people = cr.query(ContactsContract.Contacts.CONTENT_URI, null,
				null, null, null);
		ArrayList<String> temp = new ArrayList<String>();
		if (people != null && people.moveToFirst()) {
			do {
				String name = people
						.getString(people
								.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME));
				temp.add(name);
			}while(people.moveToNext());
		}
		String[] names = new String[temp.size()];
		names = temp.toArray(names);
		setListAdapter(new ArrayAdapter<String>(this, R.layout.contacts,names));

		ListView listView = getListView();
		listView.setTextFilterEnabled(true);
		
		
	}
}
